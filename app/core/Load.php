<?php
namespace App;

use Exception;

class Load
{

    public function view($name, array $vars = array())
    {
        $file = BASE_PATH . 'app/views/' . $name . '.php';

        if (is_readable($file)) {
            if (isset($vars)) {
                extract($vars);
            }
            $content = $file;
            return require(BASE_PATH . 'app/views/template.php');
        }
        throw new Exception('Не найден файл представления ' . $name . '.php ');
    }

    public function tplGet($name, array $vars = array())
    {
        $file = BASE_PATH . 'app/views/tpl/' . $name . '.php';

        if (is_readable($file)) {
            if (isset($vars)) {
                extract($vars);
            }
            ob_start();
            /** @noinspection PhpIncludeInspection */
            include($file);
            $tpl = ob_get_contents();
            ob_end_clean();
            return $tpl;
        }
        throw new Exception('Не найден файл представления ' . $name . '.php ');

    }

    public function model($name)
    {
        $model = $name;
        $modelPath = BASE_PATH . 'app/models/' . $model . '.php';

        if (is_readable($modelPath)) {
            /** @noinspection PhpIncludeInspection */
            require_once($modelPath);

            if (class_exists($model)) {
                $registry = Registry::getInstance();
                $registry->$name = new $model;
                return true;
            }
        }
        throw new Exception('Модель ' . $name . ' отсутсвует...', 404);
    }
}
