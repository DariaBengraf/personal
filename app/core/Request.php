<?php
namespace App;

/**
 * Работа с заголовком запроса
 * Class Request
 * @package App\Request\
 */
class Request
{
    /* Контроллер @var mixed|string  */
    private $_controller;

    /* Метод @var mixed|string  */
    private $_method;

    /* Параметры @var mixed|string  */
    private $_args;

    /* HTTP - request */
    private $_request;

    /* AJAX check*/
    private $_is_ajax;

    public function __construct()
    {
        $param_parts = explode('?', $_SERVER['REQUEST_URI']);
        $parts = explode('/', $param_parts[0]);
        $parts = array_filter($parts);

        $_controller = array_shift($parts);
        $this->_controller = $_controller;

        $_method = array_shift($parts);

        if ($_method == 'ajax' && isset($parts[0])) {
            $this->_method = array_shift($parts) . 'Ajax';
            $this->_is_ajax = true;
        } else {
            $this->_method = $_method . 'Action';
        }

        if (count($_POST)) {
            $this->_request['POST'] = $_POST;
        } else {
            $this->_request['POST'] = array();
        }

        if (count($_GET)) {
            $this->_request['GET'] = $_GET;
        } else {
            $this->_request['GET'] = array();
        }
    }

    /**
     * Возвращает контроллер
     * @return mixed|string
     */
    public function getController()
    {
        return ucfirst($this->_controller) ? ucfirst($this->_controller) : 'Index';
    }

    /**
     * Возвращает метод
     * @return mixed|string
     */
    public function getMethod()
    {
        return $this->_method;
    }

    /**
     * Возвращает все аргументы строкой
     * @return mixed|string
     */
    public function getArgs()
    {
        return $this->_args;
    }

    /**
     * Получение значения get-запроса
     * @param $key
     * @return mixed
     */
    public function getQuery($key = null)
    {
        if (!$key) {
            return $this->_request['GET'];
        }
        if (isset($this->_request['GET'][$key]) && $value = $this->_request['GET'][$key]) {
            return $value;
        }
        return array();
    }

    /**
     * Получение значения POST-запроса
     * @param $key
     * @return mixed
     */
    public function getPost($key = null)
    {
        if (!$key) {
            return $this->_request['POST'];
        }
        if (isset($this->_request['POST'][$key]) && $value = $this->_request['POST'][$key]) {
            return $value;
        }
        return array();
    }

    public function checkAjax()
    {
        return $this->_is_ajax;
    }
}
