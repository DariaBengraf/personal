<form id="way_points_form" name="way_points_form">
    <input type="hidden" name="car" value="<?= $car ?>"/>
    <input type="hidden" name="start" value="<?= $start ?>"/>
    <ul id="sortable" class="marker-none pointer">
        <?php foreach ($way_points as $key => $way_point): ?>
            <li data-row-id="<?= $key ?>" style="background-color: white">

                <input type="hidden" name="way_points[]"
                       value="<?= htmlspecialchars(json_encode($way_point, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_HEX_QUOT)) ?>"/>
                <div class="row">
                    <div class="col-xs-12">
                        <?php if (isset($way_point['manual']) && $way_point['manual'] == true) {
                            ; ?>
                            <h4 class="text-red">
                                <?= $way_point['name'] ?>

                                <span>
                                        *
                                    </span>
                            </h4>
                        <?php } else { ?>
                            <h4>
                                <?= $way_point['name'] ?>
                            </h4>
                        <?php }; ?>

                        <?php if (isset($way_point['manual']) && $way_point['manual'] == true) {
                            $class = 'bg-red';
                        } else {
                            $class = 'bg-primary';
                        } ?>

                        <span class="badge <?= $class; ?>">
                            <?= $way_point['lat'] ?>
                        </span>

                        <span class="badge <?= $class; ?>">
                            <?= $way_point['lng'] ?>
                        </span></br>

                        <span class="badge bg-yellow">
                            <i class="fa fa-pause"></i> <?= $way_point['pause'] ?>м
                        </span>
                        <?php if (count($way_points) > 2) { ?>
                            <span class="badge bg-red">
                                <i class="fa fa-trash pointer" onclick="admin.deletePoint(<?= $key ?>)"></i>
                            </span>
                        <?php } else {;?>
                            <span class="badge bg-grey">
                                <i class="fa fa-trash pointer"></i>
                            </span>
                        <?php }; ?>
                    </div>
                </div>
                </div>
                <hr>
            </li>
        <?php endforeach; ?>

    </ul>
</form>
<script>
    $(document).ready(
        function () {
            $("#sortable").sortable({
                stop: function (event, ui) {
                    var inp = ui.item.children()[0];
                    var json_v = JSON.parse(inp.value);
                    json_v.manual = true;
                    inp.value = JSON.stringify(json_v);
                    admin.refreshList();
                }
            });
            $("#sortable").disableSelection();
        }
    )
</script>