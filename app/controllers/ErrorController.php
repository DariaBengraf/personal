<?php
namespace App\Mvc\Controller;

/**
 * Работа с исключениями и ошибками
 * Class errorController
 */
class ErrorController extends BaseController
{
    public function error($message = 'Неизвестная ошибка')
    {
        echo '<pre>' . print_r($message, 1) . '</pre>';

    }

    public static function setError($message)
    {
        echo '<pre>' . print_r($message, 1) . '</pre>';
        return false;
    }
}
